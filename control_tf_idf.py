import tf_idf
import document
import word


class ControlTfIdf:
    def __init__(self):
        self.doc_list = []
        self.word_list = []
        tf_idf.TfIDF.clear()

    def add_doc(self, doc):
        if type(doc) is not document.Document:
            return
        self.doc_list.append(doc)
        tf_idf.TfIDF.add_doc(doc)

    def get_list_word(self):
        for doc in self.doc_list:
            for item in doc.word_in:
                tf_idf.TfIDF.calculate_tf_idf(item, doc)
        for doc in self.doc_list:
            doc.sort_list()
            doc.word_list = list(reversed(doc.word_list))
            for i in range(4):
                self.word_list.append(doc.word_list[i])
        return self.word_list


document1 = """Python is a 2000 made-for-TV horror movie directed by Richard
Clabaugh. The film features several cult favorite actors, including William
Zabka of The Karate Kid fame, Wil Wheaton, Casper Van Dien, Jenny McCarthy,
Keith Coogan, Robert Englund (best known for his role as Freddy Krueger in the
A Nightmare on Elm Street series of films), Dana Barron, David Bowe, and Sean
Whalen. The film concerns a genetically engineered snake, a python, that
escapes and unleashes itself on a small town. It includes the classic final
girl scenario evident in films like Friday the 13th. It was filmed in Los Angeles,
 California and Malibu, California. Python was followed by two sequels: Python
 II (2002) and Boa vs. Python (2004), both also made-for-TV films."""

document2 = """Python, from the Greek word (πύθων/πύθωνας), is a genus of
nonvenomous pythons[2] found in Africa and Asia. Currently, 7 species are
recognised.[2] A member of this genus, P. reticulatus, is among the longest
snakes known."""

document3 = """The Colt Python is a .357 Magnum caliber revolver formerly
manufactured by Colt's Manufacturing Company of Hartford, Connecticut.
It is sometimes referred to as a "Combat Magnum".[1] It was first introduced
in 1955, the same year as Smith &amp; Wesson's M29 .44 Magnum. The now discontinued
Colt Python targeted the premium revolver market segment. Some firearm
collectors and writers such as Jeff Cooper, Ian V. Hogg, Chuck Hawks, Leroy
Thompson, Renee Smeets and Martin Dougherty have described the Python as the
finest production revolver ever made."""

document4 = "C was originally developed by Dennis Ritchie between 1969 and 1973 at Bell Labs,[5] and used to re-implement the Unix operating system.[6] It has since become one of the most widely used programming languages of all time,[7][8] with C compilers from various vendors available for the majority of existing computer architectures and operating systems. C has been standardized by the American National Standards Institute (ANSI) since 1989 (see ANSI C) and subsequently by the International Organization for Standardization (ISO)."

doc1 = document.Document()
doc2 = document.Document()
doc3 = document.Document()
doc4 = document.Document()
doc1.string_data = document1
doc2.string_data = document2
doc3.string_data = document3
doc4.string_data = document4
test = ControlTfIdf()
test.add_doc(doc1)
test.add_doc(doc2)
test.add_doc(doc3)
test.add_doc(doc4)

for item in test.get_list_word():
    print("" + item.string + " - " + str(item.tf_idf))
