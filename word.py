class Word:
    def __init__(self, string=None, tf_idf=0):
        self.string = string
        self.tf_idf = tf_idf

    def __lt__(self, other):
        return self.tf_idf < other.tf_idf

    def __eq__(self, other):
        return self.tf_idf == other.tf_idf

    def __le__(self, other):
        return self.tf_idf <= other.tf_idf

    def __str__(self):
        return self.string
