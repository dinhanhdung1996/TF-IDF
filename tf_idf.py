import document
import word
import math


class TfIDF:
    doc_list = []

    @staticmethod
    def add_doc(doc):
        if type(doc) is not document.Document:
            return None
        else:
            TfIDF.doc_list.append(doc)
            doc.to_words()

    @staticmethod
    def calculate_tf(string, doc):
        if type(doc) is not document.Document:
            return None
        count = doc.count_word(string)
        num = doc.number_of_words()
        return count/num

    @staticmethod
    def calculate_idf(string):
        count = 0
        for doc in TfIDF.doc_list:
            if doc.contains_word(string):
                count += 1
        return math.log(float(len(TfIDF.doc_list)/count))

    @staticmethod
    def clear():
        TfIDF.doc_list.clear()

    @staticmethod
    def calculate_tf_idf(string, doc):
        if not doc.contains_word(string):
            return 0
        tf_data = TfIDF.calculate_tf(string, doc)
        idf_data = TfIDF.calculate_idf(string)
        tf_idf = tf_data * idf_data
        doc.find_word(string).tf_idf = tf_idf



